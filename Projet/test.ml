open Graph

module Test(G:Graph with type node = String.t) =
  struct
    open G

    let empty_is_empty () =
      try
        let b = G.is_empty G.empty in
        if b = true (* valeur attendue *) 
        then Printf.printf "empty_is_empty : OK\n"
        else Printf.printf "empty_is_empty : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "empty_is_empty : ERROR exception %s vs %b\n" (Printexc.to_string e) true

    let add_vertex_simple () =
      try
        let g = G.add_vertex "1" empty in
        let g2 = G.add_vertex "2" (G.add_vertex "8" g) in
        print_endline "add_vertex_simple : Graphe avec 1, 2 et 8 sans voisins";
        G.print_graph g2;
        print_newline ()
      with e ->
        Printf.printf "add_vertex_simple : ERROR exception %s vs no exception" (Printexc.to_string e)

    let add_edge_simple () =
      try
        let g = G.add_vertex "1" (G.add_vertex "2" empty) in
        let g2 = G.add_edge "1" "2" g in
        print_endline "add_edge_simple : Graphe avec 1 et 2, 2 est voisin de 1 mais pas l'inverse";
        G.print_graph g2;
        print_newline ()
      with e ->
        Printf.printf "add_edge_simple : ERROR exception %s vs no exception" (Printexc.to_string e)

    let add_edges () =
      try
        let g = G.add_vertex "3" (G.add_vertex "2" (G.add_vertex "1" (G.add_vertex "4" empty))) in
        let g2 = G.add_edge "1" "2" (G.add_edge "1" "3" (G.add_edge "2" "4" g)) in
        print_endline "add_edges : Graphe avec 1, 2, 3, 4";
        G.print_graph g2;
        print_newline ()
      with e ->
        Printf.printf "add_edges : ERROR exception %s vs no exception" (Printexc.to_string e)
    
    let add_edge_not_found () =
      try
        let g = G.add_vertex "1" empty in
        let g2 = G.add_edge "1" "2" g in
        print_endline "add_edge_not_found : Graphe avec juste 1";
        G.print_graph g2;
        print_newline ()
      with e ->
      Printf.printf "add_edge_not_found : exception %s vs no exception" (Printexc.to_string e)

    let succs_not_found () =
      try
        let g = G.add_vertex "1" empty in
        let _ = G.succs "2" g in
        Printf.printf "succs_not_found : ERROR no exception vs exception %s\n" (Printexc.to_string Not_found)
      with
        | Not_found -> print_endline "succs_not_found : OK"
        | e -> Printf.printf "succs_not_found : exception %s vs exception %s" (Printexc.to_string e) (Printexc.to_string Not_found)

    let succs_simple () =
      try
        let g = G.add_vertex "3" (G.add_vertex "2" (G.add_vertex "1" (G.add_vertex "4" empty))) in
        let g2 = G.add_edge "1" "2" (G.add_edge "1" "3" (G.add_edge "2" "4" g)) in
        let vois = G.succs "1" g2 in
        print_endline "succs_simple : Vérifications des voisins de 1. Voisins de 1 = [2, 3]. Voisins de 2 = 4";
        G.print_graph vois;
        print_newline ()
      with e ->
        Printf.printf "succs_simple : ERROR exception %s vs no exception" (Printexc.to_string e)

    let all_test () =
      let _ = empty_is_empty () in
      let _ = add_vertex_simple () in
      let _ = add_edge_simple () in
      let _ = add_edges () in
      let _ = add_edge_not_found () in
      let _ = succs_simple () in
      let _ = succs_not_found () in
      ()
  end

module Tgraph = Test(GraphAVL)

let main () =
  let _ = Printf.printf "\n === Graph AVL :: \n" in
  let _ = Tgraph.all_test () in
  ()

let _ = main ()