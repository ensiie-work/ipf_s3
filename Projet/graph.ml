module type Graph =
sig
  type node

  module NodeSet : Map.S

  type graph

  val empty : graph

  val is_empty : graph -> bool

  val add_vertex : node -> graph -> graph

  val add_edge : node -> node -> graph -> graph

  val succs : node -> graph -> graph

  val fold_node : (NodeSet.key -> graph -> 'b -> 'b) -> graph -> 'b -> 'b

  val print_graph : graph -> unit

  val list_node : graph -> node list

  val cardinal : graph -> int
  
end

module Int =
struct
  type t = int

  let compare a b = a-b
end

module GraphAVL = 
struct
  
  type node = String.t

  module NodeSet = Map.Make(String)

  type graph = G of graph NodeSet.t

  let empty = G(NodeSet.empty)

  let is_empty g =
    match g with
    | G(m) -> NodeSet.is_empty m

  let add_vertex v g = 
    match g with
    | G(m) -> if (NodeSet.mem v m) then g else G(NodeSet.add v empty m)

  let add_edge v1 v2 g =
    match g with
    | G(m) ->
    try
      let vois_v1 = NodeSet.find v1 m in
      let _ = NodeSet.find v2 m in
      G(NodeSet.add v1 (add_vertex v2 vois_v1) m)
    with Not_found ->
      g

  let cardinal g =
    match g with
    | G(m) -> NodeSet.cardinal m
  
  (* retourne le dict des voisins de v, sans avoir les voisins des voisins de v *)
  let succs2 v g =
    match g with
    | G(m) ->
    try
      let G(vois) = NodeSet.find v m in
      vois
    with Not_found -> NodeSet.empty

  (* retourne le dict des voisins de v. En appelant dict la valeur de retour,
   * les clés de dict sont les voisins de v, et les valeurs associées sont les voisins
   * des voisins de v
   *)
  let succs v g =
    match g with
    | G(m) -> 
    let G(vois_v) = NodeSet.find v m in
    let p n _ = NodeSet.mem n vois_v in
    G(NodeSet.filter p m)

  let fold_node f g e0 =
    match g with
    | G(m) -> NodeSet.fold f m e0

  (* fonction d'affichage auxiliaire *)
  let rec print_list_keys l =
    match l with
    | [] -> ()
    | (k,_)::tl -> print_string k; print_string ","; print_list_keys tl
  
  (* fonction d'affichage auxiliaire *)
  let rec print_list_assoc l =
    match l with
    | [] -> ()
    | (k,G(m))::tl -> print_string k; print_string " [ "; print_list_keys (NodeSet.bindings m);
                      print_endline " ]"; print_list_assoc tl

  let print_graph g =
    match g with
    | G(m) ->
      print_list_assoc (NodeSet.bindings m)

  let rec sep_couple l =
    match l with
    | [] -> []
    | (k,v)::tl -> k::sep_couple tl

  let list_node g =
    match g with
    | G(m) -> sep_couple (NodeSet.bindings m)


end