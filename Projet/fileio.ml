open Graph

(* Fichier d'entrée des données *)
let file =
  try
    Sys.argv.(1)
  with e ->
    failwith "Veuillez donner un fichier en argument"

let f = open_in file
let source = input_line f 
let dest = input_line f

let output_file = "out.txt"

(* Map pour stocker les distances à la source en fonction
 * du nom du noeud et de la provenance
 * pour les itérationes de l'algorithme Dijkstra
 *)
module DistSet = Map.Make(String)

(* Module de calcul principal *)
module Compute(G:Graph with type node = String.t) =
struct
  open G

  (* [print_list l]
   * Affiche une liste de string, un élément (string) par ligne
   *)
  let rec print_list l =
    match l with
    | [] -> ()
    | hd::tl -> print_endline hd; print_list tl

  (* [print_cplist l]
   * Affiche une liste de couple sous la forme a->b, un couple par ligne
   *)
  let rec print_cplist l =
    match l with
    | [] -> ()
    | (s1,s2)::tl -> print_string s1; print_string "->"; print_string s2; print_newline ();
      print_cplist tl

  (* [add_all lv g]
   * Ajoute une liste de sommets lv à un graph g
   *)
  let rec add_all lv g =
    match lv with
    | [] -> g
    | hd::tl -> add_all tl (G.add_vertex hd g)

  (* [init_dijkstra g]
   * Initialisation de l'algorithme de Dijkstra.
   * Retourne une Map pour stocker les distances de chaque noeud à la source.
   * Les distances sont initialisée à max_int. La source a une distance 0 avec elle-même.
   * Le dictionnaire stocke aussi le sommet de provenance ainsi qu'in booléen qui indique
   * si le sommet a déjà été traité
   *)
  let init_dijkstra g =
    let d = DistSet.empty in
    let f m s =
      DistSet.add s (max_int, "", false) m
    in
    let lcouple = G.list_node g in
    let tmp = List.fold_left f d lcouple in
    DistSet.add source (0,source, false) tmp

  (* [print_dist_dict d]
   * affiche le Map des distances à la source sous la forme
   * nom du sommet -> distance.
   * Fonction de debug
   *)
  let print_dist_dict d =
    let f k (dist,src,b) acc =
      print_string k;
      print_string "->";
      print_int dist;
      print_string " from ";
      print_string src;
      print_string " , visited: ";
      print_string (Bool.to_string b);
      print_newline ();
    in
    DistSet.fold f d ()

  (* [pick_min m]
   * Retourne le couple (sommet, (distance, source, booleen))
   * qui a la distance courante à la source la plus petite au sein du dictionnaire m
   *)
  let pick_min m =
    let f k v acc =
      let (node, (dist, _, _)) = acc in
      let (curr_dist, _, b) = v in
      if not b && curr_dist < dist then (k, v)
      else acc
    in
    let filtre k (_,_,b) = not b in
    DistSet.fold f m (DistSet.choose (DistSet.filter filtre m))

  (* [dijkstra m g]
   * Fonction principale de l'algorithme de dijkstra
   * Retourne un dictionnaire avec pour chaque sommet sa distance minimale à la source
   * et le sommet dont il faut provenir pour obtenir cette distance minimale.
   * m représente le dictionnaire initial
   * g représente le graph
   * n représente le nombre d'itérations à faire (noeuds non traités). n doit être >= 0
   *)
  let rec dijkstra m g n =
    if n <= 0 then m
    else begin
    (* On récupère le sommet node de distance minimale à la source *)
    let (node, (dist, src, _)) = pick_min m in
    (* On passe le sommet sélectionné à true pour dire qu'on l'a traité *)
    let m_updated = DistSet.add node (dist, src, true) m in
    (* On récupère les voisins de node *)
    let voisins = G.list_node (G.succs node g) in

    (* Fonction auxiliaire de parcours des voisins *)
    let rec aux l m2 =
      match l with
        | [] -> m2
        | hd::tl ->
          (* On récupère les informations sur le voisin courant *)
          let (curr_dist, curr_src, b) = DistSet.find hd m2 in

          (* S'il n'a pas déjà été traité et qu'on a une meilleure distance min
           * que celle existante, on met à jour *)
          if not b && dist + 1 < curr_dist
          then aux tl (DistSet.add hd (dist + 1, node, b) m2)
          else aux tl m2
    in
    let next_iter = aux voisins m_updated in
    dijkstra next_iter g (n-1)
    end

  (* [find_path m]
   * Retourne la liste des noeuds d'un plus court chemin entre la source et le puits
   * m contient les informations des distances minimales calculées avec l'algorithme de Dijkstra
   *)
  let find_path m =
    let rec aux (_,src,_) l =
      if src = source
      then source::l
      else aux (DistSet.find src m) (src::l)
    in aux (DistSet.find dest m) [dest]

  (* [main]
   * Fonction principale : lecture du fichier d'entrée et appel aux fonctions
   * de Dijkstra
   * Retourne un liste de sommet symbolisant un plus court chemin de source à dest
   *)
  let main () =

    (* on stocke dans init_graph le graph représenté dans le fichier à lire *)
    let init_graph =
    try 
      (* la source et la destination ont déjà été lus *)
      (* ajout des sommets source et destination dans un graph g *)
      let g = G.add_vertex dest (G.add_vertex source empty) in
      (* lecture du nombre de sommets du graph *)
      let _ = int_of_string (input_line f) in

      (* [aux g b]
       * Fonction auxiliaire de construction du graph à partir des données lues.
       * La fonction s'arrête lorsque la lecture lève une exception
       *)
      let rec aux g b =
        match b with
        | false -> g
        | true ->
          try
            let line = input_line f in
            let words = String.split_on_char ' ' line in
            (* ajout des 2 sommets de la ligne dans un graph gaux *)
            let gaux = add_all words g in
            (* ajout de l'arc entre les 2 sommets lus *)
            match words with
              | a::b::[] -> aux (G.add_edge a b gaux) true
              | _ -> failwith "file structre error"
            
          with e ->
            aux g false
        in

      (* stockage du graph lu *)
      let gfinal = aux g true in
      close_in f;
      (* on retourne le graph lu *)
      gfinal
    
    with e ->                    (* erreur de lecture après la fonction auxiliaire *)
      close_in_noerr f;          (* ne devrait pas arriver. init_graph contient alors un graph vide *)
      G.empty
    in

    (* appel à l'initialisation de l'algorithme de Dijkstra avec le graph lu *)
    let distDict = init_dijkstra init_graph in

    (* On récupère les voisins de la source pour la première itération de Dijkstra *)
    let vois_source =  G.list_node (G.succs source init_graph) in
    let dist_1 = List.fold_left (fun y x -> DistSet.add x (1,source,false) y ) distDict vois_source in
    (* la source est déjà traitée, on la passe à true dans le dictionnaire *)
    let dist_2 = DistSet.add source (0,source,true) dist_1 in

    (* On lance dijkstra, on a déjà traité la source donc n-1 itérations restantes avec n le nombre de noeuds *)
    let tmp = dijkstra dist_2 init_graph (G.cardinal init_graph - 1) in
    (* On retourne la liste des sommets d'un plus court chemin *)
    find_path tmp
    
end

module MCompute = Compute(GraphAVL)

let main () =
  let path = MCompute.main () in
  let oc = open_out output_file in
  (* On écrit dans out.txt le chemin trouvé par l'algorithme de Dijkstra *)
  List.iter (Printf.fprintf oc "%s ") path;
  close_out oc

let _ = main ()