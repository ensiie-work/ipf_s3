## Objectif du projet

Le sujet du projet est diponible à cette adresse : http://web4.ensiie.fr/~forest/IPF/PROJET/ipf_projet_2019.html .
L'objectif est de réaliser une implémentation des graphes en Ocaml, puis de trouver des plus courts chemins dans les graphes fournis dans des fichiers d'entrée. Des fichiers d'exemples d'entrée sont disponibles sous les noms `entree.txt`. L'exécutable écrit ensuite le résultat dans un fichier `out.txt`.

## Compilation de l'exécutable

- `make phase1` génère l'exécutable de la phase 1
- `make test` génère les tests pour l'implémentation des graphes
- `make` permet de générer les deux exécutables `test` et `phase1`
- La phase1 peut alors se lancer via `./phase1 fichier.txt`