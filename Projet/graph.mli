(** Interface pour le module de Graph *)

module type Graph =
sig
  type node

  module NodeSet : Map.S

  type graph

  (* [empty]
   * retourne un graphe vide
   *)
  val empty : graph

  (* [is_empty g]
   * retourne true si le graph g est vide, false sinon
   *)
  val is_empty : graph -> bool

  (* [add_vertex v g]
   * ajoute un noeud v à un graph g et retourne le graphe résultant
   *)
  val add_vertex : node -> graph -> graph

  (* [add_edge v1 v2 g]
   * ajoute une arrête à un graph g entre les noeuds v1 et v2
   * et retourne le graphe résultant
   * @raises Not_found si v1 ou v2 n'est pas dans g
   *)
  val add_edge : node -> node -> graph -> graph

  (* [succs n g]
   * retourne l'ensemble des successeurs d'un noeud n dans un graph g
   * @raises Not_found si n n'est pas dans g
   *)
  val succs : node -> graph -> graph

  (* [fold_note f g e0]
   * effectue un fold sur tous les noeuds du graph g avec la fonction f et l'accumulateur e0
   *)
  val fold_node : (NodeSet.key -> graph -> 'b -> 'b) -> graph -> 'b -> 'b

  (* [print_graph g] 
   * affiche les noeuds d'un graphe
   *)
  val print_graph : graph -> unit

  (* [list_node g]
   * Retourne la liste des noeuds du graph g
   *)
  val list_node : graph -> node list

  (* [cardinal g]
   * retourne le nombre de sommets dans un graph g
   *)
  val cardinal : graph -> int

end

module GraphAVL : Graph with type node = String.t
module Int : Set.OrderedType