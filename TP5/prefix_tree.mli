exception EmptyTree

exception Not_Found

module type OrderedType = 
sig
  type t

  val compare : t -> t -> int
end

module type Word = 
sig
  module Letter : OrderedType

  type word
  
  val empty : word
  (* le mot vide *)

  val is_empty : word -> bool
  (* vérifie qu'un mot est vide *)

  val add_end : word -> Letter.t -> word
  (* ajoute une lettre à la fin d'un mot *)

  val pop : word -> Letter.t * word
  (* retire la dernière lettre d'un mot et la retourne *)

end

module type TreeWord =
sig
  type tree

  type word

  module LMap : Map.S

  val is_empty : tree -> bool
    (* vérifie qu'un tree est vide *)

  val empty : tree
  (* l'ensemble vide *)

  val mem : word -> tree -> bool
  (* vérifie qu'un élément appartienne à un ensemble *)

  val add : word -> tree -> tree
  (* ajoute un élément à un ensemble. Pas de doublons *)

  val fold : (word -> 'a -> 'a) -> tree -> 'a -> 'a
  (* fold right sur l'ensemble *)

  val equal : tree -> tree -> bool
  (* vérifie que deux ensembles sont égaux *)

  val remove : word -> tree -> tree
  (* reture un élément d'un ensemble *)

  val union : tree -> tree -> tree
  (* réalise l'union entre deux ensembles *)

  val card : tree -> int
  (* retourne le nombre de mots de l'arbre = cardinal *)

  val letterCard : tree -> int
  (* retourne le nombre de lettres contenues dans l'arbre *)

  val maxWordLength : tree -> int
  (* retourne la taille du mot le plus long *)

  val addSimple : word -> tree  -> tree
  (* ajoute naïvement un mot à l'arbre *)

  val union : tree -> tree -> tree
  (* union de deux arbres *)

  val terminaisons : word -> tree -> word list
  (* retourne la liste des mots contenus en fonction d'un préfixe *)

  val listWord : tree -> word list
  (* retourne la liste de tous les mots contenus dans l'arbre *)
end

module PrefixTree(X:Word) : TreeWord with type word = X.word

module OrderedChar : OrderedType with type t = char

module WordChar(X:OrderedType) : Word with type word = X.t list