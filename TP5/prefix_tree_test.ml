open Prefix_tree

module Test(T:TreeWord) =
  struct
    open T

    let empty_is_empty () =
      try
        let b = T.is_empty T.empty in
        if b = true (* valeur attendue *) 
        then Printf.printf "empty_is_empty : OK\n"
        else Printf.printf "empty_is_empty : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "empty_is_empty : ERROR exception %s vs %b\n" (Printexc.to_string e) true

    let all_test () =
      let _ = empty_is_empty () in
      ()
  end

module Ochar = OrderedChar
module CharWord = WordChar(Ochar)
module PreTree = PrefixTree(CharWord)

module Tpretree = Test(PreTree)

let main () =
  let _ = Printf.printf "\n === PrefixTree :: \n" in
  let _ = Tpretree.all_test () in
  ()

let _ = main ()