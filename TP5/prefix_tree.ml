exception EmptyTree

exception Not_Found

module type OrderedType = 
sig
  type t

  val compare : t -> t -> int
end

module type Word = 
sig
  module Letter : OrderedType

  type word
  
  val empty : word

  val is_empty : word -> bool

  val add_end : word -> Letter.t -> word

  val pop : word -> Letter.t * word

end

module type TreeWord =
sig
  type tree

  type word

  module LMap : Map.S

  val is_empty : tree -> bool
    (* vérifie qu'un tree est vide *)

  val empty : tree
  (* l'ensemble vide *)

  val mem : word -> tree -> bool
  (* vérifie qu'un élément appartienne à un ensemble *)

  val add : word -> tree -> tree
  (* ajoute un élément à un ensemble. Pas de doublons *)

  val fold : (word -> 'a -> 'a) -> tree -> 'a -> 'a
  (* fold right sur l'ensemble *)

  val equal : tree -> tree -> bool
  (* vérifie que deux ensembles sont égaux *)

  val remove : word -> tree -> tree
  (* reture un élément d'un ensemble *)

  val union : tree -> tree -> tree
  (* réalise l'union entre deux ensembles *)

  val card : tree -> int

  val letterCard : tree -> int

  val maxWordLength : tree -> int

  val addSimple : word -> tree  -> tree

  val union : tree -> tree -> tree

  val terminaisons : word -> tree -> word list

  val listWord : tree -> word list
end

module OrderedChar = 
struct
  type t = char

  let compare a b = Char.code a - Char.code b
end

module PrefixTree(X:Word) =
struct
  type word = X.word

  module LMap = Map.Make(X.Letter)

  type tree = T of bool * tree LMap.t

  let empty = T(false, LMap.empty)

  let is_empty t =
    match t with
    | T(_,m) -> LMap.is_empty m

  let rec mem_aux w (T(b,m)) =
    if X.is_empty w
      then b
    else
      let c, w' = X.pop w in
      let s' = LMap.find c m in
      mem_aux w' s'

  let mem w t =
    try
      mem_aux w t
    with Not_found -> false

  exception Already_in

  let rec add_aux w (T(b,m)) =
    if X.is_empty w
    then
      if b then raise Already_in
      else T(true, m)
    else
      let c, w' = X.pop w in
      let s_c = try
        LMap.find c m
        with Not_Found -> empty
      in
      let s_c_w' = add_aux w' s_c in
      let m' = LMap.add c s_c_w' m in
      add_aux w (T(b,m'))
    
  let add w s =
    try add_aux w s
    with Already_in -> s
    
  let equal a = failwith "equal"
  let remove a = failwith "remove"


  let card a = failwith "card"

  let rec fold f t acc =
    match t with
    | T(_,m) -> if LMap.is_empty m then acc
      else 
      LMap.fold (fun _ t2 accu -> fold f t2 accu) m acc

  let letterCard t = failwith "lettercard"

  let maxWordLength a = failwith "maxwordlength"

  let addSimple = add

  let union a = failwith "union"

  let terminaisons a = failwith "terminaisons"

  let listWord a = failwith "listword"

end

module WordChar(X:OrderedType) = 
struct
  module Letter = X

  type word = X.t list

  let empty = []

  let is_empty w = w = []

  let rec add_end w c =
    match w with
    | [] -> [c]
    | hd::tl -> hd::add_end tl c

  let pop w =
    match w with
    | [] -> failwith "pop sur mot vide"
    | hd::tl -> (hd, tl)

end