module type Graph =
sig
  type node

  module NodeSet : Map.S

  type graph

  (* [empty]
   * graphe vide
   *)
  val empty : graph

  (* [is_empty g]
   * vérifie siun graphe est vide
   *)
  val is_empty : graph -> bool

  (* [add_vertex v g]
   * ajoute un noeud à un graph et retourne le graphe résultant
   *)
  val add_vertex : node -> graph -> graph

  (* [add_edge v1 v2 g]
   * ajoute une arrête à un graph et retourne le graphe résultant
   *)
  val add_edge : node -> node -> graph -> graph

  (* [succs n g]
   * retourne l'ensemble des successeurs d'un noeud dans un graph
   *)
  val succs : node -> graph -> graph

  (* [fold_note f g e0]
   * effectue un fold sur tous les noeuds du graph avec la fonction f
   *)
  val fold_node : (node -> 'a -> 'a) -> graph -> 'a -> 'a

  (* [print_graph g] 
   * affiche les noeuds d'un graphe
   *)
  val print_graph : graph -> unit

end

module GraphAVL : Graph with type node = int
module Int : Set.OrderedType