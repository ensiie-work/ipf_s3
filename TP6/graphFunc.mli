module type Graph =
sig
  type graph

  type noeud

  type arete

  module LMap : Map.S

  (* [nb_noeud g]
   * retourne le nombre de noeuds d'un graph donné 
   *)
  val nb_noeud : graph -> int

  (* [nb_aretes g]
   * retourne le nombre d'aretes d'un graph donné
   *)
  val nb_aretes : graph -> int

  (* [degre n g]
   * retourne le degré d'un noeud dans un graph donné
   *)
  val degre : noeud -> graph -> int

  (* [deg_max g]
   * retourne le couple du noeud qui a le plus grand degré,
   * et le degré meximal d'un graph donné
   *)
  val deg_max : graph -> (int * noeud)

  (* [transpose g]
   * retourne le transposé d'un graph donné
   *)
  val transpose : graph -> graph

  (* [union g1 g2]
   * retourne l'union de 2 graphes
   *)
  val union : graph -> graph -> graph

  (* [inter g1 g2]
   * retourne l'intersection de deux graphes
   *)
  val inter : graph -> graph -> graph

  (* [compl g]
   * retourne le complémentaire d'un graphe
   *)
  val compl : graph -> graph
end

module GraphAVL : Graph with type noeud = int with type arete = int