module type Graph =
sig
  type graph

  type noeud

  type arete

  module LMap : Map.S

  val nb_noeud : graph -> int

  val nb_aretes : graph -> int

  val degre : noeud -> graph -> int

  val deg_max : graph -> (int * noeud)

  val transpose : graph -> graph

  val union : graph -> graph -> graph

  val inter : graph -> graph -> graph

  val compl : graph -> graph
end

module GraphAVL =
struct
  type graph = 

end