module Elt =
  struct
    type t = int * char

    let compare a b =
      match a,b with
        | (i1,c1),(i2,c2) -> if i1 == i2
        then Char.code c1 - Char.code c2
        else i1 - i2
  end

module EtatSet = Map.Make(Elt)

module IntSet = Set.Make(Int)

type automate = {
  etats : IntSet.t;
  initial : int;
  acceptants : IntSet.t;
  transition : IntSet.t EtatSet.t;
}

let rec select_first l =
  match l with
  | [] -> []
  | (a,b)::tl -> b::select_first tl

let is_det a =
  let bindings_list = EtatSet.bindings a.transition in
  let values_list = select_first bindings_list in
  List.fold_left (fun x -> y -> IntSet.cardinal x == 1 && y) values_list true

type mot = char list

let build_automaton l_tripl q0 f =

  let rec aux l ens =
    match l with
    | [] -> ens
    | hd::tl -> aux tl (Set.add hd ens)
  in 
  let acc_set = aux f Set.empty in

  let rec aux2 l dic ens =
    match l with
    | [] -> (dic, ens)
    | (q,c,qp)::tl -> aux2 tl (EtatSet.add (q,c) (IntSet.add qp Set.empty) dic) (IntSet.add q (IntSet.add qp ens))
  in let (trans, ens_etats) = aux2 l_tripl EtatSet.empty Set.empty
  in {
    initial = q0;
    acceptants = acc_set;
    etats = ens_etats;
    transition = trans;
  }


exception ErrorAutomate

let execute_det a w =
  let rec aux etat dic mot =
    match mot with
    | [] -> etat
    | hd::tl -> 
      try
        let next = EtatSet.find (etat,hd) dic in
        aux next dic tl
      with _ -> raise ErrorAutomate
    in
    aux a.initial a.transition w

  let recognize a w =
    let finaux = execute a w in
    not IntSet.is_empty (IntSet.inter finaux a.acceptants)