open Int_set;;

(* Liste no exhaustive des tests à implanter: 
Forme:

"nom du test" : valeur à tester = résultat attendu 
                   ou
"nom du test" : valeur à tester raises exception attendue

"empty_is_empty" : is_empty empty = true
"empty_not_mem" : mem empty = false

"add_not_empty" : is_empty (add 3 empty) = false
"add_mem_1" : mem 3 (add 3 empty) = true
"add_mem_2" : mem 3 (add 3 (add 2 empty)) = true
"add_mem_3" : mem 2 (add 3 (add 2 empty)) = true

"get_min_empty" : get_min empty raises EmptySet



Résultat attendu à l'affichage : 

Pour chaque test : Nom du test + OK (si le test réussi) ou ERROR + valeur obtenue vs valeur attendue.

Pour l'affichage des exceptions, vous pouvez utiliser le module Printexc (https://caml.inria.fr/pub/docs/manual-ocaml/libref/Printexc.html)
*)

let empty_is_empty =
  if is_empty empty then "OK" else "ERROR";;

Printf.printf "empty_is_empty : is_empty empty = true %S\n" empty_is_empty

let empty_not_mem =
  if not (mem 1 empty) then "OK" else "ERROR";;

Printf.printf "empty_not_mem : mem empty = false %S\n" empty_not_mem

let add_not_empty = 
  if not (is_empty (add 3 empty)) then "OK" else "ERROR";;

Printf.printf "add_not_empty : is_empty (add 3 empty) = false %S\n" add_not_empty

let add_mem_1 =
  if mem 3 (add 3 empty) then "OK" else "ERROR";;

Printf.printf "add_mem_1 : mem 3 (add 3 empty) = true %S\n" add_mem_1

let add_mem_2 =
  if mem 3 (add 3 (add 2 empty)) then "OK" else "ERROR";;

Printf.printf "add_mem_3 : mem 3 (add 3 (add 2 empty)) = true %S\n" add_mem_2

let get_min_empty =
  try let tmp = get_min empty in "ERROR"
  with EmptySet -> "OK";;
  
Printf.printf "get_min_empty : get_min empty raises EmptySet %S\n" get_min_empty

let get_min_1 =
  let tmp = add 3 (add 2 empty) in
  if (get_min tmp) = 2 then "OK" else "ERROR";;

  (*
Printf.printf "get_min_1 : get_min {2, 3} = 2 %S" get_min_1

Printf.printf "equal_empty : equal empty _ = false %S" "blabl"*)