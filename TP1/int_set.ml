exception EmptySet;;

type int_set = int list;;

let empty = []

let is_empty l =
  l = []

let mem = List.mem;;

let rec add e l =
  match l with
  | [] -> [e]
  | hd::tl -> if e <= hd then e::l else hd::(add e tl)

let fold = List.fold_right

let get_min l =
  match l with
  | [] -> raise EmptySet
  | hd::tl -> hd

let rec equal l1 l2 =
  match (l1, l2) with
  | [],[] -> true
  | [],hd::tl -> false
  | hd::tl,[] -> false
  | hd1::tl1, hd2::tl2 -> hd1=hd2 && equal tl1 tl2

let rec remove e l =
  match l with
  | [] -> []
  | hd::tl -> if hd=e then tl else hd::(remove e tl)

let rec union l1 l2 = 
  match (l1,l2) with
  | [],[] -> []
  | [],hd::tl -> l2
  | hd::tl,[] -> l1
  | hd1::tl1,hd2::tl2 -> if hd1<hd2 then hd1::(union tl1 l2)
    else if hd1=hd2 then hd1::(union tl1 tl2)
    else hd2::(union l1 tl2)