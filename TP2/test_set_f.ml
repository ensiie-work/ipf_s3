open Int_set_f
(* [ compute_with_time f e] : calcule le résultat [res] de [f e] 
   et retourne le couple formé du temps de calcul et de res
 *)
let compute_with_time f e =
  let t0 = Sys.time () in
  let res = f e in
  let t1 = Sys.time () in 
  (t1-.t0,res)

(* Un jeu de test aléatoire de 1000000 ajouts/suppressions d'entier entre 0  et 999 *)
let random_add_remove =
  let test_add_remove nb val_max =
    let rec aux l n =
      if n = 0
      then List.rev l
      else
        let e = Random.int val_max in
        let fn = if Random.bool () then "add" else "remove" in 
        aux ((fn,e)::l) (n-1)
    in
    aux [] nb
  in
  test_add_remove 1000000 1000 
  

module Test(M:IntSet) =
  struct
    open M

(* Liste no exhaustive des tests à implanter: 
Forme:

"nom du test" : valeur à tester = résultat attendu 
                   ou
"nom du test" : valeur à tester raises exception attendue

"empty_is_empty" : is_empty empty = true
"empty_not_mem" : mem empty = false

"add_not_empty" : is_empty (add 3 empty) = false
"add_mem_1" : mem 3 (add 3 empty) = true
"add_mem_2" : mem 3 (add 3 (add 2 empty)) = true
"add_mem_3" : mem 2 (add 3 (add 2 empty)) = true

"get_min_empty" : get_min empty raises EmptySet



Résultat attendu à l'affichage : 

Pour chaque test : Nom du test + OK (si le test réussi) ou ERROR + valeur obtenue vs valeurs attendue.

Pour l'affichage des exceptions, vous pouvez utiliser le module Printexc (https://caml.inria.fr/pub/docs/manual-ocaml/libref/Printexc.html)
*)

    (* quelques exemples de fonctions de test *)
    let empty_is_empty () =
      try
        let b = M.is_empty M.empty in
        if b = true (* valeur attendue *) 
        then Printf.printf "empty_is_empty : OK\n"
        else Printf.printf "empty_is_empty : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "empty_is_empty : ERROR exception %s vs %b\n" (Printexc.to_string e) true

    let get_min_empty () =
      try
        let n = get_min empty in
        Printf.printf "get_min_empty : ERROR %d vs exception %s\n" n (Printexc.to_string EmptySet)
      with
      | EmptySet -> Printf.printf "get_min_empty : OK\n"
      | e -> Printf.printf "get_min_empty : ERROR exception %s vs exception %s\n" (Printexc.to_string e) (Printexc.to_string EmptySet)
  
    let random_add_remove () =
      List.fold_left (fun s (fn,d) ->
          if fn = "add"
          then add d s
          else remove d s
        ) empty random_add_remove

    let get_min_simple () = 
      try
        let t = add 5 (add 3 (add 7 empty)) in
        let n = get_min t in
        if n = 3 then
        Printf.printf "get_min_simple : OK\n"
        else
        Printf.printf "get_min_simple : ERROR %i vs %i\n" n 3
      with
      | e -> Printf.printf "get_min_simple : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let mem_empty () =
      try 
        let b = mem 2 empty in
        if not b then Printf.printf "mem_empty : OK\n"
        else Printf.printf "mem_empty : ERROR %b vs %b\n" b false
      with
      | e -> Printf.printf "mem_empty : ERROR exception %s vs %b\n" (Printexc.to_string e) false

    let mem_simple () = 
      try
        let b = mem 2 (add 2 (add 3 empty)) in
        if b then Printf.printf "mem_simple : OK\n"
        else Printf.printf "mem_simple : ERROR %b vs %b\n" b true
      with
      | e -> Printf.printf "mem_simple : ERROR exception %s vs %b\n" (Printexc.to_string e) false

    let fold_empty () =
      try
      let res = fold (fun x y -> x+y) empty 0 in
      if res = 0 then Printf.printf "fold_empty : OK\n"
      else Printf.printf "fold_empty : ERROR %i vs %i\n" res 0
    with
    | e -> Printf.printf "fold_simple : ERROR exception %s vs %i\n" (Printexc.to_string e) 6


    let fold_simple () =
      try
        let t = add 2 (add 3 (add 1 empty)) in
        let res = fold (fun x y -> x+y) t 0 in
        if res = 6 then Printf.printf "fold_simple : OK\n"
        else Printf.printf "fold_simple : ERROR %i vs %i\n" res 6
      with
      | e -> Printf.printf "fold_simple : ERROR exception %s vs %i\n" (Printexc.to_string e) 6

    let equal_simple () =
      try 
        let t1 = add 4 (add 1 (add 2 empty)) in
        let t2 = add 1 (add 4 (add 2 empty)) in
        M.print t1;
        M.print t2;
        let b = equal t1 t2 in
        if b then Printf.printf "equal_simple : OK\n"
        else Printf.printf "equal_simple : ERROR %b vs %b\n" b true
      with
      | e -> Printf.printf "equal_simple : ERROR exception %s vs %i\n" (Printexc.to_string e) 6

    let union_simple () =
      try
        let t1 = add 1 (add 2 empty) in
        let t2 = add 3 (add 4 empty) in
        let res = union t1 t2 in
        let b = mem 1 res && mem 4 res in
        if b then Printf.printf "union_simple : OK\n"
        else Printf.printf "union_simple : ERROR elemt 1 or 4 is not present\n"
      with
      | e -> Printf.printf "union_simple : ERROR exception %s vs %i\n" (Printexc.to_string e) 6


    let all_test () =
      let _ = empty_is_empty () in
      (* ........ autres tests ici *)
      let _ = get_min_empty () in
      let _ = get_min_simple () in
      let _ = mem_empty () in
      let _ = mem_simple () in
      let _ = fold_empty () in
      let _ = fold_simple () in
      let _ = equal_simple () in
      let _ = union_simple () in
      (*..... autres tests ici *)
      (* tests de temps ici *)
      (* let (random_time,_) = compute_with_time random_add_remove () in*) 
      (* afficher le temps *)
      ()
  end

module TList = Test(IntSetList)
module TAbr = Test(IntSetAbr)
module TInt = Test(IntSetIntervals)

let main () =             
  (* ajouter les tests pour les liste et les intervals *)
  let _ = Printf.printf "\n === ABR :: \n" in
  let _ = TAbr.all_test ()  in
  let _ = Printf.printf "\n === LIST :: \n" in
  let _ = TList.all_test () in
  let _ = Printf.printf "\n === INTERVALS :: \n" in
  let _ = TInt.all_test () in
  ()
    
let _ = main ()