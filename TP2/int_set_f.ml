exception EmptySet

module type IntSet =
  sig 
    type int_set
    (** type ABSTRAIT (ne pas modifier cette ligne !) des ensemble d'entiers *)

    val is_empty : int_set -> bool
      
    val empty : int_set

    val mem : int -> int_set -> bool

    val add : int -> int_set -> int_set

    val fold : (int -> 'a -> 'a) -> int_set -> 'a -> 'a

    val get_min : int_set -> int

    val equal : int_set -> int_set -> bool

    val remove : int -> int_set -> int_set

    val union : int_set -> int_set -> int_set

    val print : int_set -> unit
        
  end

module IntSetList =
 struct
  type int_set = int list

  let empty = []
  
  let is_empty l = l = []
  
  let mem = List.mem;;
  
  let rec add e l =
    match l with
    | [] -> [e]
    | hd::tl -> if e <= hd then e::l else hd::(add e tl)
  
  let fold = List.fold_right
  
  let get_min l =
    match l with
    | [] -> raise EmptySet
    | hd::tl -> hd
  
  let rec equal l1 l2 =
    match (l1, l2) with
    | [],[] -> true
    | [],hd::tl -> false
    | hd::tl,[] -> false
    | hd1::tl1, hd2::tl2 -> hd1=hd2 && equal tl1 tl2
  
  let rec remove e l =
    match l with
    | [] -> []
    | hd::tl -> if hd=e then tl else hd::(remove e tl)
  
  let rec union l1 l2 = 
    match (l1,l2) with
    | [],[] -> []
    | [],hd::tl -> l2
    | hd::tl,[] -> l1
    | hd1::tl1,hd2::tl2 -> if hd1<hd2 then hd1::(union tl1 l2)
      else if hd1=hd2 then hd1::(union tl1 tl2)
      else hd2::(union l1 tl2)

  let rec print l =
    match l with
    | [] -> Printf.printf "[]\n"
    | hd::tl -> Printf.printf "%i::" hd; print tl
 end

module IntSetAbr =
 struct 
  type int_set = Empty | Node of (int_set * int * int_set)

  let empty = Empty

  let is_empty t = t = Empty

  let rec mem e t =
    match t with
    | Empty -> false
    | Node(g,r,d) -> r = e || mem e g || mem e d

  let rec add e t =
    match t with
    | Empty -> Node(Empty, e, Empty)
    | Node(g,r,d) -> if e = r then t 
      else if e < r then Node(add e g, r, d)
      else Node(g, r, add e d)

  let rec get_min t =
    match t with
    | Empty -> raise EmptySet
    | Node(g,r,d) -> if is_empty g then r else get_min g

  let rec equal t1 t2 =
    match (t1,t2) with
    | Empty, Empty -> true
    | Empty, t | t, Empty -> false
    | Node(g1,r1,d1),Node(g2,r2,d2) -> r1=r2 && equal g1 g2 && equal d1 d2
  
  let rec fold f t e0 =
    match t with
    | Empty -> e0
    | Node(g,r,d) ->
      let tmp = fold f g e0 in
      let tmp2 = f r tmp in
      fold f d tmp2

  let rec union t1 t2 =
    match t2 with
    | Empty -> t1
    | Node(g,r,d) -> union (union (add r t1) g) d

  let rec remove e t =
    failwith "remove à compléter"

  let print a =
    failwith "print arbre"
end

module IntSetIntervals =
 struct
  type int_set = (int*int) list

  let is_empty i = i = []

  let empty = []

  let rec mem e i =
    match i with
    | [] -> false
    | (x1,x2)::tl -> 
    if e < x1
    (* e avant l'intervalle *)
      then false
    else if (e > x1 && e < x2) || (e = x1 || e = x2)
    (* e dans l'intervalle *)
      then true
    else mem e tl
    (* e après l'intervalle *)

  (* NE PRENDS PAS EN COMPTE LA REUNION SI 2 INTERVALLES ONT UN SEUL ENTIER ENTRE EUX *)
  let rec add e i =
    match i with
    | [] -> [(e,e)]
    | (x1,x2)::tl ->
    if e < x1 - 1
    (* si e est bien plus petit que le prochain intervalle *)
      then (e,e)::i
    else if e = x1 - 1
    (* si e est juste avant un intervalle *)
      then (e,x2)::tl
    else if e > x1 && e < x2
    (* si e est dans un intervalle *)
      then i
    else if e = x1 || e = x2
    (* si e est une borne d'un intervalle *)
      then i
    else if e = x2 + 1
    (* si e est juste après un intervalle *)
      then (x1,e)::tl
    else if e > x2 +1
      then (x1,x2)::add e tl
    (*  si e est bien après l'intervalle actuel *)
    else failwith "add: cas non traité"

  let fold f i e = failwith "fold"

  let get_min i =
    match i with
    | [] -> raise EmptySet
    | (x1,x2)::tl -> x1

  let rec equal i1 i2 =
    match (i1, i2) with
    | [],[] -> true
    | (_,[]) | ([],_) -> false
    | ((a1,b1)::tl1 ,(a2,b2)::tl2 ) ->
    (a1 = a2) && (b1 = b2) && (equal tl1 tl2)

  let remove e i =
    if not (mem e i) then i
    else begin
    let rec aux elt interv =
      match interv with
      | [] -> interv
      | (x1,x2)::tl ->
      if elt = x1
        then (x1+1,x2)::tl
      else if elt > x1 && elt < x2
        then (x1,elt-1)::(elt+1,x2)::tl
      else if elt = x2
        then (x1,x2-1)::tl
      else aux elt tl
    in aux e i
    end

  let union i1 i2 = failwith "union"

  let rec print i =
    match i with
    | [] -> Printf.printf "[]\n"
    | (x1,x2)::tl -> Printf.printf "(%i,%i)::" x1 x2; print tl
  


 end