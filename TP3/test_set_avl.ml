open Int_set_avl

module Test(M:Set with type elt = int) =
  struct
    open M

    let tree = M.add 5 (M.add 4 (M.add 3 (M.add 2 (M.add 1 empty))))
    let tree2 = M.add 1 (M.add 3 (M.add 2 (M.add 5 (M.add 4 empty))))
    let treediff = M.add 7 (M.add 9 (M.add 6 empty))

    let rec random_tree n start =
      if n <= 0 then start
      else random_tree (n-1) (add (Random.int 100) start)

    let empty_is_empty () =
      try
        let b = M.is_empty M.empty in
        if b = true (* valeur attendue *) 
        then Printf.printf "empty_is_empty : OK\n"
        else Printf.printf "empty_is_empty : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "empty_is_empty : ERROR exception %s vs %b\n" (Printexc.to_string e) true

    let get_min_empty () =
      try
        let b = M.get_min M.empty in
        Printf.printf "get_min_empty : ERROR %i vs exception %s\n" b (Printexc.to_string EmptySet)
      with e ->
        if e = EmptySet
        then Printf.printf "get_min_empty : OK\n"
        else Printf.printf "get_min_empty : ERROR exception %s vs %s" (Printexc.to_string e) (Printexc.to_string EmptySet)

    let mem_empty () =
      try
        let b = mem 1 M.empty in
        if b = false
        then Printf.printf "mem_empty : OK\n"
        else Printf.printf "mem_empty : ERROR %b vs %b\n" b false
      with e ->
        Printf.printf "mem_empty : ERROR exception %s vs no exception\n" (Printexc.to_string e)
    
      let height_empty () =
        try
          let res = M.height M.empty in
          if res = 0
          then Printf.printf "height_empty : OK\n"
          else Printf.printf "height_empty : ERROR %i vs %i\n" res 0
        with e ->
          Printf.printf "height_empty : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let fold_empty () = 
      try
        let res = M.fold (fun x y -> x + y) M.empty 1 in
        if res = 1
        then Printf.printf "fold_empty : OK\n"
        else Printf.printf "fold_empty : ERROR %i vs %i\n" res 1
      with e ->
        Printf.printf "fold_empty : ERROR exception %s vs no exception" (Printexc.to_string e)

    let height_simple () =
      try
        let res = M.height (M.add 1 empty) in
        if res = 1
        then Printf.printf "height_simple : OK\n"
        else Printf.printf "height_simple : ERROR %i vs %i\n" res 1
      with e ->
        Printf.printf "height_simple : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let add_complex () =
      try
        let res = M.height tree in
        if res = 3
        then begin
          Printf.printf "add_complex : OK\n";
          Printf.printf "height_complex : OK\n";
        end
        else Printf.printf "add_complex : ERROR %i vs %i\n" res 3
      with e ->
        Printf.printf "add_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let mem_complex () =
      try
        let b = mem 2 tree in
        if b
        then Printf.printf "mem_complex : OK\n"
        else Printf.printf "mem_complex : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "mem_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let fold_complex () =
      try
        let res = M.fold (fun x y -> x + y) tree 0 in
        if res = 15
        then Printf.printf "fold_complex : OK\n"
        else Printf.printf "fold_complex : ERROR %i vs %i\n" res 15
      with e ->
        Printf.printf "fold_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let equal_complex () =
      try
        let b = equal tree tree2 in
        if b
        then Printf.printf "equal_complex : OK\n"
        else Printf.printf "equal_complex : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "equal_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let equal_diff () =
      try
        let b = equal tree treediff in
        if b
        then Printf.printf "equal_diff : ERROR %b vs %b\n" b false
        else Printf.printf "equal_diff : OK\n"
      with e ->
        Printf.printf "equal_simple : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let get_min_complex () =
      try
        let res = M.get_min tree in
        if res = 1
        then Printf.printf "get_min_complex : OK\n"
        else Printf.printf "get_min_complex : ERROR %i vs %i\n" res 1
      with e ->
        Printf.printf "get_min_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

    let union_diff () = 
      try
        let res = union tree treediff in
        let b = mem 9 res in
        if b
        then Printf.printf "union_diff : OK\n"
        else Printf.printf "union_diff : ERROR %b vs %b\n" b true
      with e ->
        Printf.printf "union_diff : ERROR exception %s vs no exception" (Printexc.to_string e)

    let remove_complex () =
      try
        let t = remove 4 tree in
        let b = mem 4 t in
        if b
        then Printf.printf "remove_complex : ERROR %b vs %b\n" b false
        else Printf.printf "remove_complex : OK\n"
      with e ->
        Printf.printf "remove_complex : ERROR exception %s vs no exception\n" (Printexc.to_string e)

        let all_test () =
      let _ = empty_is_empty () in
      let _ = get_min_empty () in
      let _ = mem_empty () in
      let _ = height_empty () in
      let _ = fold_empty () in
      let _ = height_simple () in
      let _ = add_complex () in
      let _ = mem_complex () in
      let _ = fold_complex () in
      let _ = equal_complex () in
      let _ = equal_diff () in
      let _ = get_min_complex () in
      let _ = union_diff () in
      let _ = remove_complex () in
      ()

  end

module SetAvlInt = SetAvl(ElemInt)
module SetListInt = SetList(ElemInt)
module Tavl = Test(SetAvlInt)
module Tlist = Test(SetListInt)

let main () =
  let _ = Printf.printf "\n === AVL :: \n" in
  let _ = Tavl.all_test () in
  let _ = Printf.printf "=== tests AVL finished\n" in
  let _ = Printf.printf "\n === LIST :: \n" in
  let _ = Tlist.all_test() in
  let _ = Printf.printf "=== tests LIST finished\n" in
  ()

let _ = main ()