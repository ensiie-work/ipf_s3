exception EmptySet

module type Set =
  sig
    type set
    (* type générique des ensembles *) 

    type elt
    (* type générique des éléments de l'ensemble *)

    val is_empty : set -> bool
    (* vérifie qu'un set est vide *)

    val height : set -> int
    (* retourne la hauteur de l'arbre *)

    val empty : set
    (* l'ensemble vide *)

    val mem : elt -> set -> bool
    (* vérifie qu'un élément appartienne à un ensemble *)

    val add : elt -> set -> set
    (* ajoute un élément à un ensemble. Pas de doublons *)

    val fold : (elt -> 'a -> 'a) -> set -> 'a -> 'a
    (* fold right sur l'ensemble *)

    val get_min : set -> elt
    (* retourne le plus petit élément de l'ensemble *)

    val equal : set -> set -> bool
    (* vérifie que deux ensembles sont égaux *)

    val remove : elt -> set -> set
    (* reture un élément d'un ensemble *)

    val union : set -> set -> set
    (* réalise l'union entre deux ensembles *)

    val printset : set -> unit
    (* affiche un ensemble *)
        
  end

(* module d'un élément d'un ensemble *)
module type Elem =
sig
  type t
  (* déclaration du type d'un élément *)

  val compare : t -> t -> int
  (* fonction de comparaison des éléments *)

  val printelt : t -> unit
  (* affiche un élément *)
end

module SetAvl(X:Elem): Set with type elt = X.t
(* Ensembmes implémentés avec les AVL en précisant le type *)

module SetList(X:Elem): Set with type elt = X.t
(* Ensembmes implémentés avec les listes en précisant le type *)

module ElemInt : Elem with type t = int
(* Eléments d'un ensemble implémenté avec des int en précisant le type *)