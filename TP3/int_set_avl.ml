exception EmptySet

module type Set =
  sig 
    type set

    type elt

    val is_empty : set -> bool

    val height : set -> int
      
    val empty : set

    val mem : elt -> set -> bool

    val add : elt -> set -> set

    val fold : (elt -> 'a -> 'a) -> set -> 'a -> 'a

    val get_min : set -> elt

    val equal : set -> set -> bool

    val remove : elt -> set -> set

    val union : set -> set -> set

    val printset : set -> unit
        
  end

module type Elem =
  sig
    type t
    
    val compare : t -> t -> int

    val printelt : t -> unit
  end

module ElemInt =
  struct
    type t = int

    let compare i1 i2 =
      i1 - i2

    let printelt e = Printf.printf "%i" e
  end

module SetList(X:Elem) = 
 struct
  open X

  type elt = X.t

  type set = elt list

  let empty = []

  let is_empty l = l = empty

  let mem = List.mem;;
  
  let rec add e l =
    match l with
    | [] -> [e]
    | hd::tl -> if e <= hd then e::l else hd::(add e tl)
  
  let fold = List.fold_right
  
  let get_min l =
    match l with
    | [] -> raise EmptySet
    | hd::tl -> hd
  
  let rec equal l1 l2 =
    match (l1, l2) with
    | [],[] -> true
    | [],hd::tl -> false
    | hd::tl,[] -> false
    | hd1::tl1, hd2::tl2 -> hd1=hd2 && equal tl1 tl2
  
  let rec remove e l =
    match l with
    | [] -> []
    | hd::tl -> if hd=e then tl else hd::(remove e tl)
  
  let rec union l1 l2 = 
    match (l1,l2) with
    | [],[] -> []
    | [],hd::tl -> l2
    | hd::tl,[] -> l1
    | hd1::tl1,hd2::tl2 -> if hd1<hd2 then hd1::(union tl1 l2)
      else if hd1=hd2 then hd1::(union tl1 tl2)
      else hd2::(union l1 tl2)

  let rec printset l =
    match l with
    | [] -> Printf.printf "[]\n"
    | hd::tl -> X.printelt hd;
      Printf.printf "::";
      printset tl

  let height l = failwith "pas de height pour les listes\n"
 end

 (* on peut définir un SetInterval avec une fonction de comparaison
  * particulière qui prend des couples et renvoit plusieurs valeurs
  * selon les comparaisons lexicographiques 
  *)

module SetAvl(X:Elem) =
 struct
  open X

  type elt = X.t

  type set = Empty | Node of (set * elt * set * int)

  let empty = Empty

  let is_empty a = a = empty

  let height a =
    match a with
    | Empty -> 0
    | Node(_,_,_,i) -> i

  let rec mem e a =
    match a with
    | Empty -> false
    | Node(g,r,d,i) -> let tmp = X.compare e r in
    if tmp = 0 then true
    else if tmp < 0 then mem e g
    else mem e d

  let node fg r fd = 
    Node(fg,r,fd,1 + max (height fg) (height fd))

  let balance fg r fd = 
    let hg = height fg in
    let hd = height fd in
    if hg > hd + 1
    then
      match fg with
      | Empty -> assert false (* cas impossible car hg > hd+1 >= 1 *)
      | Node(fgg,rg,fgd,_) ->
        if height fgg > height fgd
        then node fgg rg (node fgd r fd)
        else match fgd with
        | Empty -> assert false (* height fgd > height fgg *)
        | Node(fgdg,rgd,fgdd,_) -> node (node fgg rg fgdg) rgd (node fgdd r fd)

    else if hd > hg + 1
    then
      match fd with
      | Empty -> assert false (* cas impossible car hd > hg + 1 >= 1 *)
      | Node(fdg, rd, fdd, _) ->
        if height fdd > height fdg
        then node (node fg r fdg) rd fdd
        else match fdg with
        | Empty -> assert false (* height fdg > height fdd *)
        | Node(fdgg,rdg,fdgd,_) -> node (node fg r fdgg) rdg (node fdgd rd fdd)

    else node fg r fd

  let rec add e a =
    match a with
    | Empty -> node empty e empty
    | Node(fg,r,fd,_) ->
      let c = X.compare e r in
      if c = 0
      then a
      else if c < 0
      then balance (add e fg) r fd
      else balance fg r (add e fd)

  let rec fold f a e0 =
    match a with
    | Empty -> e0
    | Node(g,r,d,_) ->
      let tmpg = fold f g e0 in
      let tmpr = f r tmpg in
      fold f d tmpr

  let rec get_min a =
    match a with
    | Empty -> raise EmptySet
    | Node(g,r,d,i) -> if is_empty g then r
      else get_min g

  (* vérifie que tous les éléments de a sont compris dans x *)
  let rec mem_all a x =
    match a with
    | Empty -> true
    | Node(g,r,d,_) -> mem r x
      && mem_all g x
      && mem_all d x

  (* double inclusion, implémentation lente ? *)
  let equal a1 a2 =
    mem_all a1 a2 && mem_all a2 a1

  (* TODO *)
  let rec remove e a =
    match a with
    | Empty -> raise EmptySet
    | Node(fg,r,fd,_) ->
      if not (mem e a) then a
      else let c = X.compare e r in
        if c < 0 then balance (remove e fg) r fd
        else if c > 0 then balance fg r (remove e fd)
        else
          if is_empty fd then fg
          else
          let mini = get_min fd in
          balance fg mini (remove mini fd)

  (* chaque élément de a2 est add dans a1, implémentation lente ? *)
  let rec union a1 a2 =
    match a2 with
    | Empty -> a1
    | Node(fg,r,fd,_) -> union (union (add r a1) fg) fd

  let rec printset a =
    match a with
    | Empty -> ()
    | Node(fg,r,fd,_) ->
      begin
        printset fg;
        X.printelt r;
        Printf.printf ", ";
        printset fd;
      end
 end