# [IPF] Programmation fonctionnelle - séance 2

Site web de l'encadrant [J. Forest](http://web4.ensiie.fr/~forest/IPF/)

## 1. Interface et module en Ocaml

### Interface du module

Un module défini par un type **abstrait**
```Ocaml
type abstract_type
```
et un ensemble de **signatures de fonctions**
```Ocaml
val func_name : abstract_type -> int -> bool -> ...
```
contenus dans un fichier `new_module.mli` uqi est l'intefrace du module.

### Corps du module

L'implémentation de ce module dans un fichier `new_module.ml` du même nom que l'interface du module. Cette implémentation doit contenir la définition du type abstrait `abstract_type` ainsi que l'implémentation de **toutes** les fonctions déclarées dans l'interface.

Les tests sont à rédiger **avant** ou en même temps que l'implémentation des fonctions du modules, dans un fichier `test_module.ml` par exemple.

### Chaîne de compilation

En Ocaml l'interface doit être compilée pour générer un `.cmi`
```sh
ocamlc -c new_module.mli
```
Puis le corps du module et les tests peuvent être compilés en un `.cmo`
```sh
ocamlc -c new_module.ml
ocamlc -c test_module.ml
```
Et enfin l'exécutable final `test` peut être compilé.
```sh
ocamlc new_module.cmo test_module.cmo -o test
```
> Attention à l'ordre de compilation, le corps du module **doit** être avant les fichiers qui utilisent ce module

## 2. Foncteurs et multiples implémentations d'un module

### Problématique

Selon la méthode précédente il est impossible d'implémenter de plusieurs manières au sein d'un même fichier le même module (par exemple avec des listes et des arbres). Il faudrait créer un fichier de corps par implémentation (ex: `module_liste.ml` et `module_liste.ml`), ce qui implique de créer aussi un fichier d'interface par implémentation.

Cela pose aussi le problèmes des tests, qui ne sont plus généralistes : il faut leur dire quel implémentation utiliser. Ces problèmes peuvent être résolus par les **foncteurs**.

### L'interface

L'interface `new_module.mli` va d'abord contenir la signature du module :
```Ocaml
module type MyModule:
    sig
        type abstract_type

        val func_name : type1 -> type2 -> ...
    end

```
Puis on va définir les sous-modules qui vont implémenter le module `MyModule`:
```Ocaml
module MyModuleList : MyModule
module MyModuleTree : MyModule
```

### Le corps

On va fournir dans le même fichier l'implémentation du module avec les listes **et** les arbres. Le fichier `new_module.ml` doit commencer par la signature du module, à savoir **tout le contenu de l'interface** sauf la déclaration des sous-modules (il faudra les implémenter évidemment). La structure va donc êtrre :
```Ocaml
module type MyModule:
    sig
        type abstract_type

        val func_name : type1 -> type2 -> ...
    end

module MyModuleList =
    struct
        type abstract_type = int list

        let func_name a b = ...
    end

module MyModuleTree =
    strcut
        type abstract_type = Empty | Node of (abstract_type * int * abstract_type)

        let func_name a b = ...
    end
```

### Les tests

L'objectif est d'écrire une seule série de tests qui seront lancés avec toutes les implémentations différentes du module. On commence par inclure l'interface `new_module.mli` :
```Ocaml
open New_module
```
Puis on créé un foncteur (un module qui prend en argument un sous-module) pour gérer les tests génériques :
```Ocaml
module Test(M:MyModule) =
  struct
    open M

    let test_1 () =
        if M.func_name 1 2 then ...

    ...

    let all_tests () =
        let _ = test_1 () in
        let _ = test_2 () in
        ...
        ()
  end
```
Puis on instancie des foncteurs avec nos différents sous-modules :
```Ocaml
module Mliste = Test(MyModuleList)
module Mtree = Test(MyModuleTree)
```
Et on termine par lancer tous nos tests pour chaque implémentation :
```Ocaml
let main () =             
  let _ = Printf.printf "Tests with trees :: \n" in
  let _ = Mtree.all_test ()  in
  let _ = Printf.printf "Tests with lists :: \n" in
  let _ = Mliste.all_test () in
  ()
    
let _ = main ()
```

### La chaîne de compilation

La compilation est la même que précédemment, à savoir compilation de l'interface, du corps, des tests, puis de l'exécutable en respectant l'ordre :
```sh
ocamlc -c new_module.mli new_module.ml test_module.ml
ocamlc new_module.cmo test_module.cmo -o test
```
> Les fichiers sources seront disponible sur gitlab